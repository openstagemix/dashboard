import QtQuick 2.12
import dashboard 1.0
import QtQuick.Controls 2.3
import QtQuick.Studio.Components 1.0
import QtQuick.Layouts 1.0

Rectangle {
    id: rectangle
    width: Constants.width
    height: Constants.height
    color: "#333333"

    Text {
        y: 42
        color: "#eeeeee"
        text: qsTr("Open Stage Mix Dashboard")
        font.family: "Cantarell"
        anchors.horizontalCenterOffset: 1
        anchors.horizontalCenter: parent.horizontalCenter
        horizontalAlignment: Text.AlignHCenter
        font.pointSize: 30
    }

    Channel {
        id: channel
        x: 39
        y: 151
        width: 181
        height: 520
        text: "Master"
    }

    Channel {
        id: channel1
        x: 313
        y: 151
        width: 181
        height: 520
        text: "Lead Singer 1"
    }

    Channel {
        id: channel2
        x: 500
        y: 151
        width: 181
        height: 520
        text: "Acoustics"
    }

    Channel {
        id: channel3
        x: 687
        y: 151
        width: 181
        height: 520
        text: "Drums"
    }

    Channel {
        id: channel4
        x: 874
        y: 151
        width: 181
        height: 520
        text: "Cowbells 1"
    }

    Channel {
        id: channel5
        x: 1061
        y: 151
        width: 181
        height: 520
        text: "Cowbells 2"
    }
}
