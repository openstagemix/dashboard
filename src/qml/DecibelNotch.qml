import QtQuick 2.12
import QtQuick.Layouts 1.0
import dashboard 1.0

RowLayout {
    property alias text: label.text
    Text {
        id: label
        color: "#333333"
        text: qsTr(text)
        horizontalAlignment: Text.AlignRight
        font.pixelSize: 14
    }

    Rectangle {
        id: notch
        color: "#333333"
        radius: 1
        Layout.preferredHeight: 2
        Layout.preferredWidth: 50
        z: 0
    }
}
