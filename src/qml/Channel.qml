import QtQuick 2.12
import QtQuick.Controls 2.0
import dashboard 1.0

Rectangle {
    property alias text: label.text
    id: rectangle2
    width: 180
    height: 520
    color: "#222222"
    radius: 5
    Text {
        id: label
        x: 26
        y: 23
        text: qsTr(text)
        color: "#eeeeee"
        anchors.horizontalCenterOffset: 0
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 20
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Text {
        id: element4
        x: 42
        color: "#eeeeee"
        text: qsTr("-20dB")
        anchors.horizontalCenterOffset: 0
        anchors.top: slider1.bottom
        horizontalAlignment: Text.AlignHCenter
        anchors.topMargin: 11
        font.pixelSize: 18
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Text {
        id: element5
        x: 32
        color: "#eeeeee"
        text: qsTr("80%")
        anchors.horizontalCenterOffset: 0
        anchors.top: element4.bottom
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        anchors.topMargin: 7
        font.pixelSize: 16
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Slider {
        id: slider1
        x: 37
        y: 67
        width: 40
        height: 315
        z: 2
        value: 0.5
        live: true
        anchors.horizontalCenter: parent.horizontalCenter
        orientation: Qt.Vertical
    }

    Button {
        id: button
        x: 40
        y: 462
        text: "<font color='#eeeeee'>" + qsTr("Settings") + "</font>"
        padding: 11
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: parent.horizontalCenter
        background: Rectangle {
            radius: 3
            color: "#1a1a1a"
        }
    }

    DecibelNotch {
        id: decibelNotch
        text: " 10"
        anchors.right: parent.right
        anchors.rightMargin: 63
        x: 44
        y: 67
    }

    DecibelNotch {
        id: decibelNotch1
        x: 44
        text: "0"
        anchors.top: decibelNotch.bottom
        anchors.topMargin: 15
        anchors.rightMargin: 63
        anchors.right: parent.right
    }

    DecibelNotch {
        id: decibelNotch2
        x: 44
        text: "-10"
        anchors.top: decibelNotch1.bottom
        anchors.rightMargin: 63
        anchors.topMargin: 15
        anchors.right: parent.right
    }

    DecibelNotch {
        id: decibelNotch3
        x: 52
        text: "-20"
        anchors.top: decibelNotch2.bottom
        anchors.rightMargin: 63
        anchors.topMargin: 15
        anchors.right: parent.right
    }

    DecibelNotch {
        id: decibelNotch4
        x: 53
        text: "-30"
        anchors.top: decibelNotch3.bottom
        anchors.rightMargin: 63
        anchors.topMargin: 15
        anchors.right: parent.right
    }

    DecibelNotch {
        id: decibelNotch5
        x: 54
        text: "-40"
        anchors.top: decibelNotch4.bottom
        anchors.rightMargin: 63
        anchors.topMargin: 15
        anchors.right: parent.right
    }

    DecibelNotch {
        id: decibelNotch6
        x: 45
        text: "-50"
        anchors.top: decibelNotch5.bottom
        anchors.rightMargin: 63
        anchors.topMargin: 15
        anchors.right: parent.right
    }

    DecibelNotch {
        id: decibelNotch7
        x: 51
        text: "-60"
        anchors.top: decibelNotch6.bottom
        anchors.rightMargin: 63
        anchors.topMargin: 15
        anchors.right: parent.right
    }

    DecibelNotch {
        id: decibelNotch8
        x: 44
        text: "-70"
        anchors.top: decibelNotch7.bottom
        anchors.rightMargin: 63
        anchors.topMargin: 15
        anchors.right: parent.right
    }

    DecibelNotch {
        id: decibelNotch9
        x: 53
        text: "-∞"
        anchors.top: decibelNotch8.bottom
        anchors.rightMargin: 63
        anchors.topMargin: 15
        anchors.right: parent.right
    }

    border.width: 0
}
